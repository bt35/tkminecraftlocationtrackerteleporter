# TKMineCraftLocationTrackerTeleporter

This thing allows us to track our minecraft locations, and teleport to them. It uses MCPI. 

MCPI: https://www.stuffaboutcode.com/p/minecraft-api-reference.html
Here is a Raspberry Pi Tutorial it has good context: https://projects.raspberrypi.org/en/projects/getting-started-with-minecraft-pi/4


![GUI Example](guiExample.JPG "GUI Example")
