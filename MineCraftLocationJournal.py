
"""
TK GUI for tracking a single players location in MineCraft Server and for bookmarking locations.

Location database sqlite

There are a bunch of things that need to be setup on a java edition minecraft server in order to use MCPI, such as
Raspberry Juice which should work with other servers like PaperMC. 

Picraft also exists which may be an alternative too. 
"""
# Path lib for finding paths.
from pathlib import Path
# Tkinter stuff.
from tkinter import *
from tkinter import ttk
# Date time for tracking time a location was saved. Probably don't need this.
import datetime
# Need this to save stuff in sqlite database.
import sqlite3
# MCPI API modules.
from mcpi import minecraft
from mcpi import block

# Be sure that this minecraft API client is configured to connect to your MC server. 
mc = minecraft.Minecraft.create(address = "127.0.0.1")

# This is not a good thing to do. Global...but we need this to be set by the GUI.
# Likely some TK variable would be better here.
locationValue = ()

"""
Get VEC3 player location. 
"""
def getPlayerLocation():
    """
    Getting coordinates of places and stuff
    """
    # Set the player name to the player who will be using the app.
    # The app was built with single player in mind.
    name = "yourplayernamehere"
    myPlayer = mc.getPlayerEntityId(name)

    return mc.player.getPos()


def connect(path):
    """
    Connect to the sqlite database.

    This method will create the database if it does not exist yet.

    Excepts SQLite Errors.
    """
    conn = None
    try:
        conn = sqlite3.connect(path)
        print(conn)
        print(sqlite3.version)
        return conn
    except sqlite3.Error as er:
        print("SQLite error: %s" % (" ".join(er.args)))
        print("Exception class is: ", er.__class__)
        print("SQLite traceback: ")
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))


def storeItInSqlite(note, location):
    """
    Store location in sqlite database.
    """
    # Check if the file exists
    # if exists use it
    path = Path("minecraftLocation.db")
    if path.is_file():
        # store stuff
        conn = connect(path)
        cur = conn.cursor()
        insertCommand = """INSERT INTO 'journal' (entrydate, note, x, y, z) VALUES (?,?,?,?,? );"""
        entryData = (datetime.datetime.now(), note ,location[0], location[1], location[2])
        cur.execute(insertCommand, entryData)
        conn.commit()
        cur.close()
        conn.close()
    else:
        # if not exists create it then use it.
        # Create the file
        conn = connect(path)
        cur = conn.cursor()
        cur.execute(
            "CREATE TABLE journal (id INTEGER PRIMARY KEY AUTOINCREMENT ,entrydate timestamp, note VARCHAR ,x REAL, y REAL, z REAL)"
        )
        conn.commit()
        cur.close()
        conn.close()


def getEntriesFromSqlite():
    """
    Get all the entries from the sqlite database. This is so we can use that data to populate the list box.
    """
    path = Path("minecraftLocation.db")
    if path.is_file():
        # store stuff
        conn = connect(path)
        cur = conn.cursor()
        selectCommand = """SELECT * FROM journal"""
        result = cur.execute(selectCommand)
        ret = result.fetchall()
        conn.commit()
        cur.close()
        conn.close()
        print(ret)
        return ret

def saveLocation():
    """
    Save the location.
    """
    note = textEditor.get(0.1, END)
    #print(note)
    # Get the location of the player, store it as a tuple (x,y,z)
    location = (getPlayerLocation().x, getPlayerLocation().y, getPlayerLocation().z)
    # Store it in the sqlite database
    storeItInSqlite(note,location)
    # Refresh the list box so we can see the new data.
    refreshListBox()

def onselect(event):
    global locationValue
    """
    Select the journal entry to view.
    """
    #try:
        # Note here that Tkinter passes an event object to onselect()
    selection = event.widget.curselection()
    index = selection[0]
    value = event.widget.get(index)
    # show some contextual feedback in the GUI. IE, show what we selected.
    journalViewer.delete(1.0, END)
    journalViewer.insert(
        1.0, "Location is x: {} y:{} z:{}".format(value[3], value[4], value[5]))
    # Set this global variable. We will need it for teleporting.
    locationValue = value
'''
# Maybe well use this if we can get it working... 
def updateCurrentLocation():
    # We use this to get the player's location so we can see it on the GUI.
    # Cool feedback, but ultimately unecessary.
    thing.set(getPlayerLocation())
    root.after(3000, updateCurrentLocation())
    '''

def deleteLocation():
    """
    Delete the selected entry. We use the ID to delete the entry.
    :return:
    """
    global locationValue
    #print(locationValue)
    path = Path("minecraftLocation.db")

    if path.is_file():
        # store stuff
        conn = connect(path)
        cur = conn.cursor()
        deleteCommand = """DELETE FROM 'journal' WHERE ID = {}""".format(locationValue[0])
        cur.execute(deleteCommand)
        conn.commit()
        cur.close()
        conn.close()
    # Refresh the listbox when delete finished.
    refreshListBox()

def refreshListBox():
    """Refresh the list box"""
    journalEntries.delete(0,END)
    entries = getEntriesFromSqlite()
    if entries != None:
        for i in range(len(entries)):
            journalEntries.insert(i, entries[i])

def teleportLocation():
    global locationValue
    mc.player.setPos(locationValue[3],locationValue[4],locationValue[5])
    print("Teleported to: {},{},{}".format(locationValue[3],locationValue[4],locationValue[5]))


"""
TKInter Graphics Stuff Below.
"""

root = Tk()
thing = StringVar()
thing.set("Not Updated Yet")
# Main text editor box
editorLabel = Label(root, text="Current Location")
warningLabel = Label(root, text="Be careful clicking this button. \nIt will delete the selected location.")
currentLocation = Label(root, textvariable=thing)

# Location Note:
textEditor = Text(root, width=10, height=10)

# list box for journal entries
journalEntriesLabel = Label(root, text="saved Locations")
journalEntries = Listbox(root, width=42, height=10, name="saved Locations")
refreshListBox()
journalViewer = Text(root, width=42, height=10)

saveBtn = Button(root, text="Save Location?", command=saveLocation)
deleteBtn = Button(root, text="Delete Location?", command=deleteLocation, bg="#ff4747")
teleportBtn = Button(root, text="Teleport to Location?", command=teleportLocation)

editorLabel.grid(column=0, row=0, sticky=(N, S))
currentLocation.grid(column=0, row=1, sticky=(N, S, E, W), padx=10, pady=10)
textEditor.grid(column=0, row=2, sticky=(N, S, E, W), padx=10, pady=10)
saveBtn.grid(column=0, row=3, sticky=(N, S, E, W), padx=10, pady=10)
warningLabel.grid(column=2 , row=2, sticky=(N,S,E,W), padx=10, pady=10)
deleteBtn.grid(column=2, row=3, sticky=(N,S,E,W), padx=10, pady=10)
teleportBtn.grid(column=1, row=3, sticky=(N,S,E,W), padx=10, pady=10)
journalEntriesLabel.grid(column=1, row=0, sticky=(N, S, E, W))
journalEntries.grid(column=1, row=1, sticky=(N, S, E, W), padx=10, pady=10)
journalViewer.grid(column=1, row=2, sticky=(N, S, E, W), padx=10, pady=10)


root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.rowconfigure(0, weight=0)
root.rowconfigure(1, weight=1)
root.rowconfigure(2, weight=1)
root.rowconfigure(3, weight=1)
root.rowconfigure(4, weight=1)

journalEntries.bind("<<ListboxSelect>>", onselect)
root.title("Test")

root.mainloop()
